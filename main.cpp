#include <iostream>

int main()
{
	static size_t count = 100;
	std::cout << "Hello!";
	if (count-- == 0)
	{
		return 0;
	}
	return main();
}
